# PACEMAP

I want to be able to plot the GPX tracks from my runs, but in a "better" way than
my running app. I want to make the line color indicate my speed and I want to be
able to plot 2 tracks side by side, to visualize my progress.

